#
# Various utilities to render a beautiful & useful planning.
#
module PlanningHelper
  def planning(date = Date.current, category = Category.first)
    Planning.new(self, date, category).table
  end

  #
  # Contains the whole logic of drawing a Planning.
  #
  # TODO: Refactor
  #
  class Planning
    attr_accessor :view, :date, :category

    def initialize(view, date, category)
      @view, @date, @category = view, date, category
    end

    delegate :link_to, :calendars_path, :content_tag, to: :view

    def day_range
      date - 7.days..date + 7.days
    end

    def first_day
      day_range.first
    end

    def last_day
      day_range.last
    end

    def table
      content_tag :table, class: 'table calendar' do
        table_header + table_body
      end
    end

    def table_header
      content_tag :thead do
        header_year { |date| date.year.to_s } +
          header_month { |date| date.strftime('%B') } +
          header_day { |date| date.day.to_s }
      end
    end

    def day_span(start_date, end_date, res = nil)
      range = [start_date, first_day].max..[end_date, last_day].min
      return '' if range.count == 0

      return content_tag(:td, colspan: range.count) { '' }.html_safe unless res

      class_name = res.channel.underscore.gsub(' ', '-')
      content_tag(:td, colspan: range.count, class: class_name) do
        link_to res.code, res
      end.html_safe
    end

    def first_edge(rental)
      return '' if rental.arrival <= first_day

      day_span(first_day, rental.arrival - 1.day).html_safe
    end

    def last_edge(rental)
      if rental.departure > last_day
        return day_span(rental.arrival, last_day, rental.reservation)
      end

      day_span(rental.arrival, rental.departure, rental.reservation) +
        day_span(rental.departure + 1.day, last_day)
    end

    def middle_edges(rentals)
      c = ''

      rentals[0...-1].each_with_index do |r, i|
        c << day_span(r.arrival, r.departure, r.reservation)
        c << day_span(r.departure + 1.day, rentals[i + 1].arrival - 1.day)
      end

      c.html_safe
    end

    def rentals_for_row(rentals)
      return day_span(first_day, last_day) unless rentals.any?

      first_r, last_r = rentals.first, rentals.last
      result = first_edge(first_r) + middle_edges(rentals) + last_edge(last_r)
      result.html_safe
    end

    def table_row(room, number)
      title = room.virtual? ? "OB#{number + 1}" : "AP#{number + 1}"
      content_tag(:tr) do
        content_tag(:td, title) +
          rentals_for_row(room.rentals.overlaps(day_range)) +
          content_tag(:td, '')
      end
    end

    def table_body
      content_tag :tbody do
        body = ''
        category.rooms.where(virtual: false).each_with_index do |room, i|
          body << table_row(room, i)
        end
        category.rooms.where(virtual: true).each_with_index do |room, i|
          body << table_row(room, i)
        end
        body.html_safe
      end
    end

    #
    # FIXME: header_day is inefficient? Is it worth the mantainability win?
    #
    %w(year month day).each do |unit|
      define_method(:"header_#{unit}") do |&block|
        content_tag :tr do
          content_tag(:th) do
            link_to('',
                    calendars_path(date: date, move: 'prev', period: unit),
                    class: 'glyphicon glyphicon-chevron-left',
                    id: "prev-#{unit}")
          end +

            day_range.group_by { |day| day.send(unit.to_sym) }.map do |_, ds|
              content_tag(:th, colspan: ds.count) { block.call(ds.first) }
            end.join.html_safe +

            content_tag(:th) do
              link_to('',
                      calendars_path(date: date, move: 'next', period: unit),
                      class: 'glyphicon glyphicon-chevron-right',
                      id: "next-#{unit}")
            end
        end
      end
    end
  end
end

#
# Custom view helpers
#
module ApplicationHelper
  BOOTSTRAP_FLASH_MSG = {
    success: 'alert-success',
    error: 'alert-error',
    alert: 'alert-block',
    notice: 'alert-success'
  }

  def bootstrap_class_for(flash_type)
    BOOTSTRAP_FLASH_MSG.fetch(flash_type.to_sym, flash_type.to_s)
  end

  def link_to_add_rental(name, f)
    new_rental = f.object.rentals.build(category: Category.first.id)
    id = new_rental.object_id
    fields = f.fields_for(:rentals, new_rental, child_index: id) do |builder|
      render('rental_fields', f: builder)
    end
    link_to(name, '', class: 'add-fields',
                      data: { id: id, fields: fields.gsub("\n", '') })
  end

  def active_if(options)
    'active' if params.merge(options) == params
  end
end

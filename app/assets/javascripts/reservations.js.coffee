# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
jQuery ->
  enable_datepicker = (node) ->
    node.find('[id^=reservation_rentals][id$=arrival]').datepicker
      dateFormat: 'dd/mm/yy'
    node.find('[id^=reservation_rentals][id$=departure]').datepicker
      dateFormat: 'dd/mm/yy'

  location_changes = (node) ->
    node.find('[id^=reservation_rentals][id$=location]').change ->
      id = $(this).attr('id')
      rent_id = id.match(/reservation_rentals_attributes_(\d+)_location/)[1]
      $.ajax
        url: '/locations/' + $(this).val() + '?rental_id=' + parseInt(rent_id)
        dataType: 'script'
        async: true

  $('form').on 'click', '.remove-fields', (event) ->
    $(this).prev('input[type=hidden]').val('1')
    $(this).closest('.rental').hide()
    if $('#rentals .rental:visible').size() <= 1
      $('#rentals .rental:visible .remove-fields').hide()
    event.preventDefault

  $('form').on 'click', '.add-fields', (event) ->
    time = new Date().getTime()
    regexp = new RegExp($(this).data('id'), 'g')
    $(this).before($(this).data('fields').replace(regexp, time))
    if $('#rentals .rental:visible').size() > 1
      $('#rentals .rental:visible .remove-fields').show()
    enable_datepicker($('#rentals .rental'))
    location_changes($('#rentals .rental'))
    event.preventDefault()

  enable_datepicker($('#rentals .rental'))
  location_changes($('#rentals .rental'))

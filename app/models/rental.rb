#
# Represents a stay in a (generic) room
#
class Rental < ActiveRecord::Base
  belongs_to :reservation
  validates :reservation, presence: true

  belongs_to :room

  def category=(id)
    @category = Category.find(id)
  end

  def category
    room ? room.category : @category
  end

  before_validation :_set_available_room

  delegate :location, to: :room, allow_nil: true

  validates :quantity, presence: true,
                       numericality: { only_integer: true, greater_than: 0 }

  validates :arrival, :departure, presence: true
  validate :_arrival_before_departure

  validates :adults, :children, :babies,
            presence: true,
            numericality: { only_integer: true, greater_than_or_equal_to: 0 }

  scope :overlaps, lambda { |range|
    where("arrival <= '#{range.last}' AND departure >= '#{range.first}'")
  }

  def days
    (departure - arrival).to_i
  end

  def price
    category.price * days
  end

  default_scope { order(:arrival) }

  private

  def _set_available_room
    self.room ||= @category.available_room(arrival, departure)
  end

  def _arrival_before_departure
    return unless arrival.present? && departure.present? && departure < arrival

    errors.add(:arrival, 'la llegada es después de la salida!')
  end
end

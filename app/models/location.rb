#
# Represents a property used for leasing
#
class Location < ActiveRecord::Base
  validates :name, presence: true, uniqueness: true

  has_many :categories
  has_many :rooms, through: :categories

  def available_room(category_id, arrival, departure)
    category = categories.find(category_id)
    category.available_room(arrival, departure)
  end
end

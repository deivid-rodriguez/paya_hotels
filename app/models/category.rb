#
# Represents a type of Room
#
class Category < ActiveRecord::Base
  TYPES = ['Apartamento', 'Atico', 'Estudio', 'Doble', 'Habitacion D',
           'Habitacion T', 'Junior Suite', 'Standard', 'Suite', 'Superior']

  validates :name, inclusion: { in: TYPES }

  validates :price, numericality: { greater_than: 0 }

  has_many :rooms

  belongs_to :location
  validates :location, presence: true

  def available_room(arrival, departure)
    rooms.each do |room|
      return room if room.available?(arrival..departure)
    end

    rooms.create(virtual: true)
  end
end

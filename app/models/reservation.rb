#
# Represents a client reservation
#
class Reservation < ActiveRecord::Base
  CHANNELS = ['Paya', 'Booking', 'FHG', 'Primera Viaggi', 'Formentera Online',
              'FWeb', 'ITS', 'bodas', 'FRooms', 'Islamar', 'Paya Online',
              'Europlayas', 'FLibre', 'Trasmapi', 'Jumbo Tours', 'Fnatural',
              'Agencias', 'Roulette', 'ThomasCook']

  validates :channel, inclusion: { in: CHANNELS }

  validates :name, length: { maximum: 100 }

  has_many :rentals, inverse_of: :reservation, dependent: :destroy
  accepts_nested_attributes_for :rentals, allow_destroy: true

  validates :price, numericality: { greater_than_or_equal_to: 0 }

  def rentals_attributes=(attrs)
    super(attrs.each { |_k, v| v.delete('location') })
  end

  default_scope { order(created_at: :desc) }
end

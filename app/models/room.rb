#
# Represents an accomodation unit (appartment, hotel room, etc)
#
class Room < ActiveRecord::Base
  has_many :rentals
  belongs_to :category

  delegate :location, to: :category

  def available?(range = [Date.current])
    rentals.overlaps(range).any? ? false : true
  end
end

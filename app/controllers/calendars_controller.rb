#
# Controller for planning related stuff
#
class CalendarsController < ApplicationController
  before_action :_set_date

  def index
    @category, period = Category.first, params['period'].presence || 'day'

    @date = _move_date(@date, period.to_sym, params['move'])
  end

  private

  def _set_date
    @date ||= \
      params['date'] ? Date.strptime(params['date'], '%Y-%m-%d') : Date.current
  end

  def _move_date(date, scale, direction)
    return date unless %w(next prev).include?(direction)

    steps = scale == :day ? 15 : 1
    offset = steps.send(scale)

    direction == 'next' ? date + offset : date - offset
  end
end

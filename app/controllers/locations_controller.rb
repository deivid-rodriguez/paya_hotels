#
# Handles actions related to Location model.
#
class LocationsController < ApplicationController
  respond_to :js

  def show
    @location = Location.find(_location_params)
  end

  private

  def _location_params
    params.require(:id)
  end
end

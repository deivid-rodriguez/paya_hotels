#
# Most important controller in a Reservation Management app
#
class ReservationsController < ApplicationController
  before_action :_set_reservation, only: [:show, :edit, :update, :destroy]
  before_action :_set_location, only: [:new, :create]

  # GET /reservations
  def index
    @reservations = Reservation.page(params[:page])
  end

  # GET /reservations/1
  def show
  end

  # GET /reservations/new
  def new
    @reservation = Reservation.new
    @reservation.rentals.build(category: Category.first)
  end

  # POST /reservations
  def create
    @reservation = Reservation.new(_permitted_params)

    @reservation.save ? redirect_to(reservations_path) : render(action: :new)
  end

  # GET /reservations/1/edit
  def edit
  end

  # PUT /reservations/1
  def update
    if @reservation.update(_permitted_params)
      redirect_to(@reservation, notice: 'Reserva actualizada')
    else
      render(action: :edit)
    end
  end

  # DELETE /reservations/1
  def destroy
    @reservation.destroy
    redirect_to reservations_url, notice: 'Reserva borrada'
  end

  private

  def _set_reservation
    @reservation = Reservation.find(params[:id])
  end

  def _set_location
    location_id = params[:location]
    @location = location_id ? Location.find(location_id) : Location.first
  end

  def _permitted_params
    params.require(:reservation)
      .permit(:code, :channel, :name, :price, rentals_attributes: [
        :id, :location, :category, :quantity, :arrival, :departure, :adults,
        :children, :babies, :_destroy])
  end
end

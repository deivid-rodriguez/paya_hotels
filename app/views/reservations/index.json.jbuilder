json.array!(@reservations) do |reservation|
  json.extract! reservation, :id, :code, :name, :price
  json.url reservation_url(reservation, format: :json)
end

require 'rails_helper'

RSpec.describe 'Calendar', type: :feature do
  let(:room) { create(:room) }

  before { visit root_path }

  describe 'Main View' do
    it 'shows a calendar with current reservations' do
      @rental = create(:rental, room: room)
      click_link 'Planning'
      expect(page).to have_content(@rental.reservation.code)
    end
  end

  describe 'Moving Around Dates' do
    def rental_in_time(arrival, departure)
      create(:rental, room: room, arrival: arrival, departure: departure)
    end

    let!(:r1) { rental_in_time(Date.current, Date.tomorrow) }

    context 'when changing years' do
      let!(:r2) { rental_in_time(Date.current + 1.year, Date.current + 366) }

      it 'shows rental in the correct time period' do
        click_link 'Planning'
        expect(page).to have_content(r1.reservation.code)
        click_link 'next-year'
        expect(page).to have_content(r2.reservation.code)
        click_link 'prev-year'
        expect(page).to have_content(r1.reservation.code)
      end
    end

    context 'when changing months' do
      let!(:r2) { rental_in_time(Date.current + 1.month, Date.current + 32) }

      it 'shows rental in the correct time period' do
        click_link 'Planning'
        expect(page).to have_content(r1.reservation.code)
        click_link 'next-month'
        expect(page).to have_content(r2.reservation.code)
        click_link 'prev-month'
        expect(page).to have_content(r1.reservation.code)
      end
    end

    context 'when changing days' do
      let!(:r2) { rental_in_time(Date.current + 15.days, Date.current + 17) }

      it 'shows rental in the correct time period' do
        click_link 'Planning'
        expect(page).to have_content(r1.reservation.code)
        click_link 'next-day'
        expect(page).to have_content(r2.reservation.code)
        click_link 'prev-day'
        expect(page).to have_content(r1.reservation.code)
      end
    end
  end
end

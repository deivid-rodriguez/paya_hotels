require 'rails_helper'

RSpec.describe 'Reservation', type: :feature do
  let!(:reservation) { create(:rental).reservation }

  describe 'listing' do
    it 'correctly shows list of reservations' do
      visit root_path
      expect(page.html).to match(/#{reservation.code}/).and \
                           match(/#{reservation.name}/).and \
                           match(/#{reservation.price}/)
    end
  end

  describe 'showing' do
    it 'correctly displays reservation details' do
      visit root_path
      click_link reservation.code

      expect(page).to have_content("Reserva #{reservation.id}")
    end
  end

  describe 'editing' do
    before do
      visit root_path
      click_link "edit-#{reservation.id}"
    end

    it 'correctly changes reservation details' do
      fill_in 'reservation_price', with: reservation.price + 1
      click_button 'Actualizar Reserva'

      expect(page).to have_content('Reserva actualizada')
      expect(page).to have_content(reservation.price + 1)
    end

    it "correctly changes rental's reservation details" do
      children = all(:css, '[id^=reservation_rentals_][id$=_children]').last
      children.select(reservation.rentals.last.children + 1)
      click_button 'Actualizar Reserva'

      expect(page).to have_content('Reserva actualizada')
      expect(page).to have_content(reservation.rentals.last.children + 1)
    end
  end

  describe 'deleting' do
    it 'correctly removes reservation from the list' do
      visit root_path
      click_link "delete-#{reservation.id}"
      expect(page).to have_content('Reserva borrada')
    end
  end

  describe 'creation' do
    let(:reservation) { build(:reservation) }

    def fill_rental
      tomorrow, next_week = Date.tomorrow, Date.tomorrow + 1.week
      arrival = all(:css, '[id^=reservation_rentals_][id$=_arrival]').last
      arrival.set(tomorrow.strftime('%d/%m/%Y'))

      dep = all(:css, '[id^=reservation_rentals_][id$=_departure]').last
      dep.set(next_week.strftime('%d/%m/%Y'))
    end

    def fill_reservation_form
      fill_in 'reservation_code', with: reservation.code
      fill_in 'reservation_name', with: reservation.name
      fill_rental
      fill_in 'reservation_price', with: reservation.price
    end

    describe 'with standard form' do
      before { create(:category) }

      it 'correctly creates reservation and goes back to reservation list' do
        visit root_path
        click_link 'Nueva Reserva'
        fill_reservation_form
        click_button 'Crear Reserva'
        expect(page).to have_selector('table', text: reservation.code)
      end

      it 'correctly stays in the page and shows reservation errors' do
        visit root_path
        click_link 'Nueva Reserva'
        click_button 'Crear Reserva'
        expect(page).to have_selector('form#new_reservation')
      end
    end

    describe 'with multiple rentals form', js: true do
      before { create(:category) }

      it 'can create multiple rentals for the same reservation' do
        visit root_path
        click_link 'Nueva Reserva'
        fill_reservation_form
        click_link 'Añadir Unidad'
        fill_rental
        click_button 'Crear Reserva'

        expect(page).to have_css('table tbody tr', count: 2)
      end
    end
  end
end

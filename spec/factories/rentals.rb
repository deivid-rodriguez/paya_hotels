FactoryGirl.define do

  factory :rental do
    category
    room
    reservation
    arrival { Date.tomorrow }
    departure { (arrival || Date.tomorrow) + 3.days }
    quantity 1
    adults 2
    children 0
    babies 0
  end

end

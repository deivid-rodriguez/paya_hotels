FactoryGirl.define do

  factory :reservation do
    sequence(:code) { |n| format('P%07d', n) }
    channel 'Paya'
    name 'Enma'
    price 999
  end

end

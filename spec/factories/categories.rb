FactoryGirl.define do

  factory :category do
    name 'Apartamento'
    location
    price '9.99'
  end

end

FactoryGirl.define do

  factory :location do
    sequence(:name) { |n| "Paya #{n}" }
  end

end

require 'rails_helper'

RSpec.describe Location, type: :model do
  let(:location) { create(:location) }

  it 'has a name' do
    expect(build(:location, name: nil)).not_to be_valid
  end

  it 'has a non empty name' do
    expect(build(:location, name: '')).not_to be_valid
  end

  it 'has a unique name' do
    expect(build(:location, name: location.name)).not_to be_valid
  end

  describe '#available_room' do
    before do
      @category  = create(:category, name: 'Apartamento', location: location)
      @room = create(:room, category: @category)
      @arrival, @depart = Date.current, Date.current + 1.week
    end

    it 'gives an existing room if there is one available' do
      expect do
        @avail_room = location.available_room(@category.id, @arrival, @depart)
      end.not_to change(Room, :count)

      expect(@avail_room).to eq(@room)
    end

    it 'gives a new room if there is no availability' do
      create(:rental, room: @room, arrival: @arrival, departure: @depart)

      expect do
        @room = location.available_room(@category.id, @arrival, @depart)
      end.to change(Room, :count).by(1)
    end
  end
end

require 'rails_helper'

RSpec.describe Room, type: :model do

  describe '#available?' do
    let(:room) { create(:room) }

    describe 'without an argument' do
      it 'gives true when no rentals at all for the room' do
        expect(room.available?).to eq(true)
      end

      it 'gives false when rentals found for the day' do
        create(:rental, room: room, arrival: Date.current)
        expect(room.available?).to eq(false)
      end

      it 'gives true when no rentals current for the room' do
        create(:rental, room: room, arrival: Date.tomorrow)
        expect(room.available?).to eq(true)
      end
    end

    describe 'with a specific date range' do
      let(:range) { Date.current..Date.current + 1.week }

      it 'returns true when no rentals are found during that range' do
        create(:rental, room: room, arrival: 3.days.ago, departure: 1.day.ago)
        expect(room.available?(range)).to eq(true)
      end

      it 'returns false when overlapping rentals are found in that range' do
        create(:rental, room: room, arrival: Date.tomorrow)
        expect(room.available?(range)).to eq(false)
      end
    end
  end
end

require 'rails_helper'

RSpec.describe Category, type: :model do

  it 'belongs to a location' do
    expect(build(:category, location: nil)).not_to be_valid
  end

  it 'has a valid name' do
    expect(build(:category, name: 'stupid')).not_to be_valid
  end

  it 'has a numerical price' do
    expect(build(:category, price: 'muy caro')).not_to be_valid
  end

  it 'has a positive price' do
    expect(build(:category, price: -200)).not_to be_valid
  end

end

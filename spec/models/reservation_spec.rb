require 'rails_helper'

RSpec.describe Reservation, type: :model do
  it 'can have a nil code' do
    expect(build(:reservation, code: nil)).to be_valid
  end

  it 'has a valid channel' do
    expect(build(:reservation, channel: 'stupid')).not_to be_valid
  end

  it 'has a name no longer than 100 characters' do
    expect(build(:reservation, name: 'A' * 101)).not_to be_valid
  end

  it 'has a numerical price' do
    expect(build(:reservation, price: 'very expensive')).not_to be_valid
  end

  it 'has a positive or zero price' do
    expect(build(:reservation, price: 1)).to be_valid
    expect(build(:reservation, price: 0)).to be_valid
    expect(build(:reservation, price: -1)).not_to be_valid
  end
end

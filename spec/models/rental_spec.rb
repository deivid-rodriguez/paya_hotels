require 'rails_helper'

RSpec.describe Rental, type: :model do
  let(:today) { Date.current }

  it 'belongs to a reservation ' do
    expect(build(:rental, reservation: nil)).not_to be_valid
  end

  it 'has an integer number of units' do
    expect(build(:rental, quantity: 'bla')).not_to be_valid
    expect(build(:rental, quantity: 5.4)).not_to be_valid
  end

  it 'has a positive number of units' do
    expect(build(:rental, quantity: 0)).not_to be_valid
    expect(build(:rental, quantity: -1)).not_to be_valid
  end

  it 'has an arrival date' do
    expect(build(:rental, arrival: nil)).not_to be_valid
  end

  it 'has a departure date' do
    expect(build(:rental, departure: nil)).not_to be_valid
  end

  it 'has arrival date prior to departure date' do
    expect(build(:rental, arrival: today + 1.week,
                          departure: today + 6.days)).not_to be_valid
  end

  [:adults, :children, :babies].each do |person|
    it "has an integer number of #{person}" do
      expect(build(:rental, person => 'hi')).not_to be_valid
      expect(build(:rental, person => 4.5)).not_to be_valid
    end

    it "has a non negative number of #{person}" do
      expect(build(:rental, person => -1)).not_to be_valid
      expect(build(:rental, person => 0)).to be_valid
    end
  end

  describe '#days' do
    it 'calculates the length in time of the rental' do
      rental = build(:rental, arrival: today, departure: Date.tomorrow)
      expect(rental.days).to eq(1)
    end
  end

  describe '#price' do
    it 'calculates the total price of the rental' do
      room = create(:room, category: create(:category, price: 100))
      rental = build(:rental, arrival: today, departure: today + 7, room: room)
      expect(rental.price).to eq(700)
    end
  end

  describe '#_set_available_room' do
    it 'asks rental category for an available room if room not already set' do
      rental = build(:rental, room: nil)
      expect(rental.category).to receive(:available_room)
      rental.send(:_set_available_room)
    end

    it 'returns room attribute if already set' do
      rental = build(:rental)
      expect(rental.send(:_set_available_room)).to eq(rental.room)
    end
  end

  describe 'overlaps scope' do
    let(:rental) do
      create(:rental, arrival: today, departure: today + 1.week)
    end

    it 'gives a rental when provided range overlap with it' do
      range = Date.current + 1.day..Date.current + 1.month
      expect(Rental.overlaps(range)).to include(rental)
    end

    it 'gives a rental when provided range overlap in the edge with it' do
      range = Date.current + 1.week..Date.current + 1.month
      expect(Rental.overlaps(range)).to include(rental)
    end

    it 'excludes a rental when provided range does not overlap with it' do
      range = Date.current + 8.days..Date.current + 1.month
      expect(Rental.overlaps(range)).to be_empty
    end
  end
end

source 'https://rubygems.org'
ruby '2.1.5'

# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '4.1.8'
gem 'rails-i18n', '4.0.3'

# Use postgresql as the database for Active Record
gem 'pg', '0.17.1'

# Use Bootstrap with SASS for CSS stylesheets
gem 'sass-rails', '4.0.4'
gem 'bootstrap-sass', '~> 3.3.0'
gem 'autoprefixer-rails', '~> 4.0.0'

# Javascript stuff
gem 'jquery-rails', '3.1.2'    # javascript library
gem 'jquery-ui-rails', '5.0.2' # javascript UI library
gem 'coffee-rails', '4.1.0'    # Use CoffeeScript in .js.coffee assets & views
gem 'uglifier', '2.5.3'        # Compressor for JavaScript assets
gem 'jbuilder', '2.2.5'        # Build JSON APIs with ease

# Better forms
gem 'simple_form', '3.1.0.rc2'

# bundle exec rake doc:rails generates the API under doc/api.
gem 'sdoc', '0.4.1', group: :doc

# Template engine
gem 'haml-rails', '0.5.3'

# Web server
gem 'unicorn-rails', '2.2.0'

# Importing excel files
gem 'roo', '1.13.2'

# Pagination
gem 'kaminari', '0.16.1'

group :development, :test do
  gem 'rspec-rails', '3.1.0'
  gem 'factory_girl_rails', '4.5.0'
  gem 'dotenv-rails', '1.0.2'
  gem 'byebug', '3.5.1'
end

group :development do
  gem 'spring', '1.1.3'
  gem 'spring-commands-rspec', '1.0.2'
  gem 'pry', '0.10.1'
  gem 'pry-doc', '0.6.0'
end

group :test do
  gem 'capybara', '2.4.4'
  gem 'capybara-webkit', '1.3.1'
  gem 'database_cleaner', '1.3.0'
  gem 'launchy', '2.4.3'
end

group :production do
  gem 'rails_12factor', '0.0.3'
end

# Use ActiveModel has_secure_password
# gem 'bcrypt', '~> 3.1.7'

# Use Capistrano for deployment
# gem 'capistrano-rails', group: :development

# Static code analyzer
gem 'rubocop', '0.27.1', require: false

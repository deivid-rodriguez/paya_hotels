require 'roo'

namespace :db do
  FIRST_ROW = 6
  LAST_ROW = 239

  COLUMN_MAPPINGS = {
    code: 'C', location: 'D', category: 'G', quantity: 'H', name: 'I',
    adults: 'J', children: 'K', babies: 'L', arrival: 'N', departure: 'O',
    price: 'Q', channel: 'V'
  }

  def get_row_data(spreadsheet, row)
    data = {}
    COLUMN_MAPPINGS.keys.each do |k|
      data[k] = spreadsheet.cell(row, COLUMN_MAPPINGS[k])
    end
    data
  end

  desc 'Load sample data from excel file'
  task :populate do
    abort('You need to specify an excel file') unless ENV['file']

    s = Roo::Excel.new(File.new(ENV['file']).path, file_warning: :ignore)
    s.default_sheet = s.sheets.first

    (FIRST_ROW..LAST_ROW).each do |i|
      data = get_row_data(s, i)

      code = data[:code].is_a?(Numeric) ? data[:code].to_i.to_s : data[:code]
      if code.present?
        res = Reservation.find_or_initialize_by(code: code) do |r|
          r.channel = data[:channel]
          r.name = data[:name]
          r.price = data[:price]
        end
      else
        res = Reservation.new(data.slice(:channel, :name, :price))
      end

      location = Location.find_by(name: data[:location])
      category = location.categories.find_by(name: data[:category])
      res.rentals.build(category: category.id,
                        quantity: data[:quantity].to_i,
                        adults: data[:adults].to_i,
                        children: data[:children].to_i,
                        babies: data[:babies].to_i,
                        arrival: data[:arrival],
                        departure: data[:departure])
      res.save!
    end
  end
end

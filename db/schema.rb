# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20140829165640) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "categories", force: true do |t|
    t.integer  "location_id", null: false
    t.string   "name",        null: false
    t.decimal  "price"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "categories", ["location_id", "name"], name: "index_categories_on_location_id_and_name", unique: true, using: :btree

  create_table "locations", force: true do |t|
    t.string "name"
  end

  create_table "rentals", force: true do |t|
    t.integer "room_id",                    null: false
    t.integer "reservation_id",             null: false
    t.integer "quantity",       default: 1
    t.date    "arrival",                    null: false
    t.date    "departure",                  null: false
    t.integer "adults",         default: 2, null: false
    t.integer "children",       default: 0, null: false
    t.integer "babies",         default: 0, null: false
  end

  create_table "reservations", force: true do |t|
    t.string   "code",       default: ""
    t.string   "name"
    t.decimal  "price"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "channel",    default: "paya", null: false
  end

  create_table "rooms", force: true do |t|
    t.integer "category_id"
    t.boolean "virtual",     default: false, null: false
  end

end

class CreateReservations < ActiveRecord::Migration
  def change
    create_table :reservations do |t|
      t.string :code
      t.string :hotel
      t.string :accomodation
      t.string :name
      t.datetime :arrival
      t.datetime :departure
      t.integer :adults
      t.integer :children
      t.integer :babies
      t.decimal :price

      t.timestamps
    end
  end
end

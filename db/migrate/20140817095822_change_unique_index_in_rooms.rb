class ChangeUniqueIndexInRooms < ActiveRecord::Migration
  def up
    add_index :rooms, [:location_id, :kind, :number], unique: true
  end

  def down
    remove_index :rooms, [:location_id, :kind, :number]
  end
end

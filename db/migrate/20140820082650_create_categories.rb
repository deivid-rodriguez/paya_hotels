class CreateCategories < ActiveRecord::Migration
  def change
    create_table :categories do |t|
      t.integer :location_id, null: false
      t.string :name, null: false
      t.decimal :price

      t.timestamps
    end

    add_index :categories, [:location_id, :name], unique: true

    remove_column :rooms, :kind, :string
    remove_column :rooms, :location_id, :integer
    remove_column :rooms, :price, :decimal
    remove_column :rooms, :number, :integer

    add_column :rooms, :category_id, :integer
    add_column :rooms, :virtual, :boolean, null: false, default: false
  end
end

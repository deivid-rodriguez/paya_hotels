class RemoveIndexFromRentals < ActiveRecord::Migration
  def up
    remove_index :rentals, [:room_id, :reservation_id]
  end

  def down
    add_index :rentals, [:room_id, :reservation_id]
  end
end

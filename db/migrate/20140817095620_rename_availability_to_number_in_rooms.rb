class RenameAvailabilityToNumberInRooms < ActiveRecord::Migration
  def change
    rename_column :rooms, :availability, :number
  end
end

class CreateRooms < ActiveRecord::Migration
  def change
    create_table :rooms do |t|
      t.string :kind, null: false
      t.integer :location_id, null: false
      t.decimal :price
      t.integer :availability
    end

    remove_column :reservations, :accomodation, :string
  end
end

class MoveAdultsChildrenBabiesToRentals < ActiveRecord::Migration
  def up
    add_column :rentals, :adults, :integer, default: 2, null: false
    add_column :rentals, :children, :integer, default: 0, null: false
    add_column :rentals, :babies, :integer, default: 0, null: false

    remove_column :reservations, :adults, :integer
    remove_column :reservations, :children, :integer
    remove_column :reservations, :babies, :integer
  end

  def down
    add_column :reservations, :adults, :integer
    add_column :reservations, :children, :integer
    add_column :reservations, :babies, :integer

    remove_column :rentals, :adults, :integer
    remove_column :rentals, :children, :integer
    remove_column :rentals, :babies, :integer
  end
end

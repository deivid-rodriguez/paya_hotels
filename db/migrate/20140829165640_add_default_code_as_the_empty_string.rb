class AddDefaultCodeAsTheEmptyString < ActiveRecord::Migration
  def up
    change_column_default :reservations, :code, ''
  end

  def down
    change_column_default :reservations, :code, nil
  end
end

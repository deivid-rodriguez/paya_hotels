class CreateRentals < ActiveRecord::Migration
  def change
    create_table :rentals do |t|
      t.integer :room_id, null: false, index: true
      t.integer :reservation_id, null: false, index: true
      t.integer :quantity, default: 1

      t.date :arrival, null: false, index: true
      t.date :departure, null: false, index: true

      t.index [:room_id, :reservation_id], unique: true
    end

    remove_column :reservations, :hotel, :string
    remove_column :reservations, :arrival, :date
    remove_column :reservations, :departure, :date
  end
end

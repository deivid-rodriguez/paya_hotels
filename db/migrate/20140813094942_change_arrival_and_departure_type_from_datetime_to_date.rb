class ChangeArrivalAndDepartureTypeFromDatetimeToDate < ActiveRecord::Migration
  def up
    change_column :reservations, :arrival, :date, null: false
    change_column :reservations, :departure, :date, null: false
  end

  def down
    change_column :reservations, :arrival, :datetime, null: false
    change_column :reservations, :departure, :datetime, null: false
  end
end

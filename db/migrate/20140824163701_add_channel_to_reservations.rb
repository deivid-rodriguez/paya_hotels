class AddChannelToReservations < ActiveRecord::Migration
  def change
    add_column :reservations, :channel, :string, default: 'Paya', null: false
  end
end

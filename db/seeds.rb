#
# Creates a <hotel> with the specific <room_data> passed in.
#
def create_hotel(name, room_data)
  h = Location.find_or_create_by(name: name)
  room_data.each do |r|
    cat = h.categories.find_or_create_by(name: r[:cat], price: r[:pri])
    (r[:avail] - cat.rooms.count).times do
      cat.rooms.create
    end
  end
end

create_hotel('Paya', [{ avail: 6, cat: 'Apartamento', pri: 190 }])

create_hotel('Paya II', [{ avail: 6, cat: 'Apartamento', pri: 190 },
                         { avail: 4, cat: 'Estudio', pri: 190 }])

create_hotel('Carmen', [{ avail: 4, cat: 'Apartamento', pri: 190 }])

create_hotel('Portu Saler', [{ avail: 17, cat: 'Apartamento', pri: 190 },
                             { avail: 4, cat: 'Atico', pri: 190 }])

create_hotel('Cala Pujols', [{ avail: 12, cat: 'Habitacion D', pri: 190 },
                             { avail: 3, cat: 'Habitacion T', pri: 206 }])

create_hotel('Blanco', [{ avail: 12, cat: 'Standard', pri: 190 },
                        { avail: 17, cat: 'Superior', pri: 206 },
                        { avail: 7, cat: 'Junior Suite', pri: 338 },
                        { avail: 2, cat: 'Suite', pri: 390 }])
